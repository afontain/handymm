#!/usr/bin/env bash

# handymm/codegen/generate_defs_and_docs.sh

# Global environment variables:
# GMMPROC_GEN_SOURCE_DIR  Top directory where source files are searched for.
#                         Default value: $(dirname "$0")/../..
#                         i.e. 2 levels above this file.
# GMMPROC_GEN_BUILD_DIR   Top directory where built files are searched for.
#                         Default value: $GMMPROC_GEN_SOURCE_DIR
#
# If you use jhbuild, you can set these environment variables equal to jhbuild's
# configuration variables checkoutroot and buildroot, respectively.
# Usually you can leave GMMPROC_GEN_SOURCE_DIR undefined.
# If you have set buildroot=None, GMMPROC_GEN_BUILD_DIR can also be undefined.

# Generated files:
#   handymm/handy/src/handy_docs.xml
#   handymm/handy/src/handy_enums.defs
#   handymm/handy/src/handy_methods.defs
#   handymm/handy/src/handy_signals.defs

# Root directory of handymm source files.
root_dir="$(dirname "$0")/.."

# Where to search for source files.
if [ -z "$GMMPROC_GEN_SOURCE_DIR" ]; then
  GMMPROC_GEN_SOURCE_DIR="$root_dir/.."
fi

# Where to search for built files.
if [ -z "$GMMPROC_GEN_BUILD_DIR" ]; then
  GMMPROC_GEN_BUILD_DIR="$GMMPROC_GEN_SOURCE_DIR"
fi

# Scripts in glibmm. These are source files.
gen_docs="$GMMPROC_GEN_SOURCE_DIR/glibmm/tools/defs_gen/docextract_to_xml.py"
gen_methods="$GMMPROC_GEN_SOURCE_DIR/glibmm/tools/defs_gen/h2def.py"
gen_enums="$GMMPROC_GEN_SOURCE_DIR/glibmm/tools/enum.pl"

# Where to find the executable that generates extra defs (signals and properties).
extra_defs_gen_dir="$GMMPROC_GEN_BUILD_DIR/handymm/tools/extra_defs_gen"
### If handymm is built with meson:
if [ "$GMMPROC_GEN_SOURCE_DIR" == "$GMMPROC_GEN_BUILD_DIR" ]; then
  # handymm is built with meson, which requires non-source-dir builds.
  # This is what jhbuild does, if necesary, to force non-source-dir builds.
  extra_defs_gen_dir="$GMMPROC_GEN_BUILD_DIR/handymm/build/tools/extra_defs_gen"
fi
### If handymm is built with autotools:
# handymm is built with autotools.
# autotools support, but don't require, non-source-dir builds.

source_prefix="$GMMPROC_GEN_SOURCE_DIR/handy"
build_prefix="$GMMPROC_GEN_BUILD_DIR/handy"
### If handy is built with meson:
if [ "$source_prefix" == "$build_prefix" ]; then
  # handy is built with meson, which requires non-source-dir builds.
  # This is what jhbuild does, if neccesary, to force non-source-dir builds.
  build_prefix="$build_prefix/build"
fi
### If handy is built with autotools:
# handy is built with autotools, which support, but don't require, non-source-dir builds.

out_dir="$root_dir/handy/src"

# Documentation
echo === handy_docs.xml ===
params="--with-properties --no-recursion"
for dir in "$source_prefix/handy" "$build_prefix/handy"; do
  if [ -d "$dir" ]; then
    params="$params -s $dir"
  fi
done
"$gen_docs" $params > "$out_dir/handy_docs.xml"

shopt -s nullglob # Skip a filename pattern that matches no file

# Enums
echo === handy_enum.defs ===
"$gen_enums" "$source_prefix"/handy/*.h "$build_prefix"/handy/*.h  > "$out_dir/handy_enums.defs"

# Functions and methods
echo === handy_method.defs ===
"$gen_methods" "$source_prefix"/handy/*.h "$build_prefix"/handy/*.h  > "$out_dir/handy_methods.defs"

# Properties and signals
echo === handy_signal.defs ===
"$extra_defs_gen_dir"/generate_defs_handy > "$out_dir/handy_signals.defs"


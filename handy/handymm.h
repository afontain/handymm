/* Copyright (c) 2019  Joe Hacker <joe@example.com>
 *
 * This file is part of handymm.
 *
 * handymm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * handymm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HANDYMM_H_INCLUDED
#define HANDYMM_H_INCLUDED

/** @mainpage handymm Reference Manual
 *
 * @section description Description
 *
 * The handymm C++ binding provides a C++ interface on top of libhandy,
 * a C library
 *
 * @section overview Overview
 *
 * [...]
 *
 * @section use Use
 *
 * To use handymm in your C++ application, include the central header file
 * <tt>\<handymm.h\></tt>.  The handymm package ships a @c pkg-config
 * file with the correct include path and link command-line for the compiler.
 */

#include <handymmconfig.h>
#include <handymm/handy.h>

/** @example example/example.cc
 * A handymm example program.
 */

#endif /* !HANDYMM_H_INCLUDED */

/* Copyright (c) 2019  Joe Hacker <joe@example.com>
 *
 * This file is part of handymm.
 *
 * handymm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * handymm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

_DEFS(handymm,handy)
_CONFIGINCLUDE(handymmconfig.h)
_PINCLUDE(glibmm/private/object_p.h)

#define HANDY_USE_UNSTABLE_API
#define _HANDY_INSIDE
#include <hdy-enums.h>

namespace Hdy
{

	namespace Arrows {
		_WRAP_ENUM(Direction, HdyArrowsDirection, s#^ARROWS_##)
	}

	namespace Fold {
		_WRAP_ENUM(Fold, HdyFold, s#^FOLD_##)
	}

	_WRAP_ENUM(CenteringPolicy, HdyCenteringPolicy)

	namespace Leaflet {
		_WRAP_ENUM(TransitionType, HdyLeafletTransitionType, s#^LEAFLET_##)
		_WRAP_ENUM(ModeTransitionType, HdyLeafletModeTransitionType, s#^LEAFLET_##)
	}

	namespace Paginator {
		_WRAP_ENUM(IndicatorStyle, HdyPaginatorIndicatorStyle, s#^PAGINATOR_##)
	}

	namespace Squeezer {
		_WRAP_ENUM(TransitionType, HdySqueezerTransitionType, s#^SQUEEZER_##)
	}

	namespace ViewSwitcher {
		_WRAP_ENUM(Policy, HdyViewSwitcherPolicy, s#^VIEW_SWITCHER_##)
	}

} // namespace Hdy
